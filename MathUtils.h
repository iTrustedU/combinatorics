#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#include <boost/smart_ptr.hpp>
#include <boost/assert.hpp>
#include <boost/foreach.hpp>
#include "AlphabetFactory.h"
#include <iostream>
#include <vector>
/*
 * Combinatorics namespace.
 * Useful data structures and algorithms.
 */
namespace math_cmbcs
{

const int MAX_VAR_N = 100;

template<class T>
class AbstractElement {

public:
	virtual ~AbstractElement() {};
	AbstractElement() : value(T()), ordinal(0) {}
	friend std::ostream& operator<<(std::ostream& out, const AbstractElement<T>& rVal) {
		out<<rVal.value;
		return out;
	}

	friend bool operator<(const AbstractElement<T>& left, const AbstractElement<T>& right) {
		return left.ordinal < right.ordinal;
	}

	friend bool operator>(const AbstractElement<T>& left, const AbstractElement<T>& right) {
		return left.ordinal > right.ordinal;
	}

	friend bool operator==(const AbstractElement<T>& left, const AbstractElement<T>& right) {
		return left.ordinal == right.ordinal;
	}

	friend bool operator!=(const AbstractElement<T>& left, const AbstractElement<T>& right) {
		return left.ordinal != right.ordinal;
	}

	void Assign(T rVal, int ord) {
		ordinal = ord;
		value = rVal;
	}

	int GetOrdinal() {
		return ordinal;
	}

private:
	T value;
	int ordinal;
};

/*
 * Variation
 */
template<class T>
class Variation {

public:
	friend std::ostream& operator<<(std::ostream& out, const Variation<T>& rVal) {
		BOOST_FOREACH(T cT, rVal.set)
			out<<cT;
		return out;
	}

	void Insert(T val) {
		set.push_back(val);
	}

	void Assign(int pos, T val) {
		BOOST_ASSERT(pos < Size());
		set[pos] = val;
	}

	T Get(int pos) const {
		BOOST_ASSERT(pos < Size());
		return set[pos];
	}

	int DigitsSum() {
		int sum = 0;
		BOOST_FOREACH(T cT, set)
				sum+=cT.GetOrdinal();
		return sum;
	}
	int Size() const { return set.size(); }
private:
	std::vector<T> set;
};

/*
 * Alphabet
 */
template<class T>
class Alphabet {

public:
	T GetFirstLetter() const {
		return letters.front();
	}

	T GetLastLetter() const {
		return letters.back();
	}

	void InsertAlpha(T alpha) {
		letters.push_back(alpha);
	}

	bool IsMember(const T& tPar) const {
		return (std::find(letters.begin(), letters.end(), tPar) != letters.end());
	}

	const std::vector<T>& GetAlphabet() const {
		return letters;
	}

	std::vector<Variation<T> > GenerateVariations(int k)
	{
		std::vector<Variation<T> > list;
		Variation<T> var;
		SetFirstVariation(var, k);
		list.push_back(var);
		while(!IsLastVariation(var)) {
			SetNextVariation(var);
			list.push_back(var);
		}
		return list;
	}

protected:
	bool IsLastVariation(const Variation<T>& var) {
		for(int i=0; i < var.Size(); ++i) {
			if(var.Get(i) != GetLastLetter())
				return false;
		}
		return true;
	}

	void SetFirstVariation(Variation<T>& var, int k) {
		for(int i=0; i < k; ++i) {
			var.Insert(GetFirstLetter());
		}
	}

	void AssignLastVariation(Variation<T>& var) {
		for(int i=0; i < var.Size(); ++i) {
			var.Assign(i, GetLastLetter());
		}
	}

	T GetNextLetter(int ord) const {
		BOOST_ASSERT(ord < GetLastLetter().GetOrdinal());
		return letters[ord+1];
	}

	void SetNextVariation(Variation<T>& var) {
		int i = var.Size()-1;
		while(var.Get(i)==GetLastLetter()) {
			var.Assign(i,GetFirstLetter());
			i--;
		}
		var.Assign(i, GetNextLetter(var.Get(i).GetOrdinal()));
	}

private:
	std::vector<T> letters;
};

}
#endif
