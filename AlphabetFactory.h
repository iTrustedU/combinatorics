#ifndef ALPHABET_FACTORY_H
#define ALPHABET_FACTORY_H
#include "MathUtils.h"
#include <boost/assert.hpp>

typedef math_cmbcs::AbstractElement<char> CharElement;
typedef math_cmbcs::AbstractElement<int> NumElement;
typedef math_cmbcs::Alphabet<CharElement > CharAlphabet;
typedef math_cmbcs::Alphabet<NumElement > NumAlphabet;

//Za neke tipove (int, char .. etc)
template<typename T>
math_cmbcs::Alphabet<math_cmbcs::AbstractElement<T> > RangedAlphabet(T from, T to) {
	BOOST_ASSERT(static_cast<T>(from)<=static_cast<T>(to));
	math_cmbcs::Alphabet<math_cmbcs::AbstractElement<T> > alphT;
	math_cmbcs::AbstractElement<T> ch;
	to++;
	int i = 0;
	while(from != to) {
		//mora imati int conversion op
		ch.Assign(from, i); from++; 
		alphT.InsertAlpha(ch);
		i++;
	}
	return alphT;
};

#endif

